/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import javax.swing.JOptionPane;

/**
 *
 * @author dfloresalarcon
 */
public class Calculadora {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        float n1, n2;
        Calculadora funciones = new Calculadora();
        n1 = Integer.parseInt(JOptionPane.showInputDialog("Primer número"));
        n2 = Integer.parseInt(JOptionPane.showInputDialog("Segundo número"));
        
        funciones.mostrarMultiplicacion(n1, n2);
         
        funciones.mostrarDivision(n1, n2);
        
       
    }
    
    public float division(float n1, float n2)
    {
        float division = 0;
        if (n1>n2) 
        {            
            switch (Integer.parseInt(JOptionPane.showInputDialog("El primer número es mayor, \n"
                    + "qué desea hacer?"
                    + "\n1- Continuar con la operación"
                    + "\n2- Invertir números")))
            {
                default:
                    JOptionPane.showMessageDialog(null, "Opcion incorrecta","ERROR!!!!!", JOptionPane.INFORMATION_MESSAGE);
                case 1:
                    break;
                case 2:
                    float auxn;
                    auxn = n2;
                    n2 = n1;
                    n1 = auxn;
                    break;
            }
        }
        return n1/n2;
    }
    
    public void mostrarDivision(float n1, float n2)
    {
        float resultado = division(n1,n2);
        JOptionPane.showMessageDialog(null, resultado , "División", JOptionPane.INFORMATION_MESSAGE);        
    }
    
    public float multiplicacion(float n1, float n2)
    {
        return n1*n2;
    }
    
    public void mostrarMultiplicacion(float n1, float n2)
    {
        float resultado = multiplicacion(n1,n2);
        JOptionPane.showMessageDialog(null, resultado , "Multiplicación", JOptionPane.INFORMATION_MESSAGE);        
    }
}
